package com.azim.wheelcolortest

import android.graphics.Color

object Utils {

    fun getArgb(color: Int, alpha: Float): Int {
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb((alpha * 255).toInt(), red, green, blue)
    }

    fun getColorAlpha(color: Int): Float {
        return Color.alpha(color) / 255f
    }

}