package com.azim.wheelcolortest

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.slider.Slider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ColorWheel.OnColorWheelChange, View.OnClickListener, Slider.OnChangeListener {

    private var colorBlock1 = Color.parseColor("#00c2a3")
    private var colorBlock2 = Color.parseColor("#4ba54f")
    private var colorBlock3 = Color.parseColor("#ff6100")
    private var selectedBlock = Block.ONE

    enum class Block {
        ONE, TWO, THREE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        v_color_wheel.onColorWheelChange = this

        v_block_1.setOnClickListener(this)
        v_block_2.setOnClickListener(this)
        v_block_3.setOnClickListener(this)

        s_brightness.addOnChangeListener(this)

        v_color_1.setCardBackgroundColor(colorBlock1)
        v_color_2.setCardBackgroundColor(colorBlock2)
        v_color_3.setCardBackgroundColor(colorBlock3)

        setSelectedBlock(selectedBlock)
    }

    private fun setSelectedBlock(selectedBlock: Block) {
        this.selectedBlock = selectedBlock
        val selectedColor: Int
        when (selectedBlock) {
            Block.ONE -> {
                v_block_1.isSelected = true
                v_block_2.isSelected = false
                v_block_3.isSelected = false
                selectedColor = colorBlock1
            }
            Block.TWO -> {
                v_block_1.isSelected = false
                v_block_2.isSelected = true
                v_block_3.isSelected = false
                selectedColor = colorBlock2
            }
            Block.THREE -> {
                v_block_1.isSelected = false
                v_block_2.isSelected = false
                v_block_3.isSelected = true
                selectedColor = colorBlock3
            }
        }

        v_color_wheel.post {
            v_color_wheel.setSelectedColor(selectedColor)
        }

        s_brightness.post {
            s_brightness.value = Utils.getColorAlpha(selectedColor)
        }

    }

    private fun updateColorBlock(color: Int, alpha: Float = 1f) {
        when (selectedBlock) {
            Block.ONE -> {
                colorBlock1 = Utils.getArgb(color, alpha)
                v_color_1.setCardBackgroundColor(colorBlock1)
            }
            Block.TWO -> {
                colorBlock2 = Utils.getArgb(color, alpha)
                v_color_2.setCardBackgroundColor(colorBlock2)
            }
            Block.THREE -> {
                colorBlock3 = Utils.getArgb(color, alpha)
                v_color_3.setCardBackgroundColor(colorBlock3)
            }
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.v_block_1 -> {
                setSelectedBlock(Block.ONE)
            }
            R.id.v_block_2 -> {
                setSelectedBlock(Block.TWO)
            }
            R.id.v_block_3 -> {
                setSelectedBlock(Block.THREE)
            }
        }
    }

    override fun onColorChange(color: Int) {
        val alpha = s_brightness.value
        updateColorBlock(color, alpha)
    }

    override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) {
        when (selectedBlock) {
            Block.ONE -> {
                updateColorBlock(colorBlock1, value)
            }
            Block.TWO -> {
                updateColorBlock(colorBlock2, value)
            }
            Block.THREE -> {
                updateColorBlock(colorBlock3, value)
            }
        }
    }
}