package com.azim.wheelcolortest

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class ThumbView @JvmOverloads constructor(
    private val parent: View,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(parent.context, attrs, defStyleAttr) {

    companion object {
        private const val THUMB_RADIUS = 50f
    }

    private var thumbColor: Int = 0
    private var cx = 0f
    private var cy = 0f

    fun setThumbCoordinate(x: Float, y: Float) {
        cx = x
        cy = y
        parent.invalidate()
    }

    fun setThumbColor(color: Int): Int {
        thumbColor = color
        parent.invalidate()
        return thumbColor
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawShadow(canvas)
        drawMainPicker(canvas)
        drawStroke(canvas)
    }

    private fun drawMainPicker(canvas: Canvas) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = thumbColor
            strokeWidth = 1f
            style = Paint.Style.FILL
        }

        canvas.drawCircle(cx, cy, THUMB_RADIUS, paint)
    }

    private fun drawShadow(canvas: Canvas) {
        val shadowPaint = Paint()
        shadowPaint.setShadowLayer(15f, 0f, 0f, Color.DKGRAY)
        shadowPaint.color = -0xaaaaab

        canvas.drawCircle(cx, cy, THUMB_RADIUS, shadowPaint)
    }

    private fun drawStroke(canvas: Canvas) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply { strokeWidth = 5f }

        paint.color = Color.WHITE
        paint.style = Paint.Style.STROKE

        canvas.drawCircle(cx, cy, THUMB_RADIUS, paint)
    }
}