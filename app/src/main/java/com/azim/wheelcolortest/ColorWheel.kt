package com.azim.wheelcolortest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.lang.Math.PI
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.hypot
import kotlin.math.sin

class ColorWheel @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        private val HUE_COLORS = intArrayOf(
            Color.RED,
            Color.YELLOW,
            Color.GREEN,
            Color.CYAN,
            Color.BLUE,
            Color.MAGENTA,
            Color.RED
        )

        private val SATURATION_COLORS = intArrayOf(
            Color.WHITE,
            Color.argb(0,
                Color.red(Color.WHITE),
                Color.green(Color.WHITE),
                Color.blue(Color.WHITE))
        )
    }

    private var wheelRadius = 0
    private var wheelCenterX = 0
    private var wheelCenterY = 0
    private val thumbView = ThumbView(this)
    var onColorWheelChange: OnColorWheelChange? = null

    private val wheel = GradientDrawable().apply {
        gradientType = GradientDrawable.SWEEP_GRADIENT
        shape = GradientDrawable.OVAL
        colors = HUE_COLORS
    }

    private val saturationGradient = GradientDrawable().apply {
        gradientType = GradientDrawable.RADIAL_GRADIENT
        shape = GradientDrawable.OVAL
        colors = SATURATION_COLORS
    }

    // Override methods

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minDimension = minOf(
            MeasureSpec.getSize(widthMeasureSpec),
            MeasureSpec.getSize(heightMeasureSpec)
        )

        setMeasuredDimension(
            resolveSize(minDimension, widthMeasureSpec),
            resolveSize(minDimension, heightMeasureSpec)
        )
    }

    override fun onDraw(canvas: Canvas) {
        drawWheel(canvas)
        thumbView.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> updateThumb(event)
            MotionEvent.ACTION_MOVE -> updateThumb(event)
        }

        return true
    }

    private fun updateThumb(event: MotionEvent) {
        parent.requestDisallowInterceptTouchEvent(true)
        updateWheelThumb(event.x, event.y)
    }

    private fun drawWheel(canvas: Canvas) {
        wheelCenterX = width / 2
        wheelCenterY = height / 2
        wheelRadius = maxOf(minOf(width, height) / 2, 0)

        val left = wheelCenterX - wheelRadius
        val top = wheelCenterY - wheelRadius
        val right = wheelCenterX + wheelRadius
        val bottom = wheelCenterY + wheelRadius

        wheel.setBounds(left, top, right, bottom)
        saturationGradient.setBounds(left, top, right, bottom)
        saturationGradient.gradientRadius = wheelRadius.toFloat()

        wheel.draw(canvas)
        saturationGradient.draw(canvas)
    }

    // Calculation for getting hue and saturation
    private fun updateWheelThumb(eventX: Float, eventY: Float) {
        val legX = eventX - wheelCenterX
        val legY = eventY - wheelCenterY
        val hue = getHue(legX, legY)
        val saturation = getSaturation(legX, legY)

        setThumbCoordinate(hue, saturation)
        val thumbColor = thumbView.setThumbColor(getColor(hue, saturation))

        onColorWheelChange?.onColorChange(thumbColor)
    }

    private fun getHue(legX: Float, legY: Float): Float {
        return ((atan2(legY, legX) * 180f / PI.toFloat()) + 360) % 360
    }

    private fun getSaturation(legX: Float, legY: Float): Float {
        val hypot = minOf(hypot(legX, legY), wheelRadius.toFloat())
        return hypot / wheelRadius
    }

    // Calculation for getting coordinate
    private fun setThumbCoordinate(hue: Float, saturation: Float) {
        val r = saturation * wheelRadius
        val hueRadians = hue / 180f * PI.toFloat()
        val x = cos(hueRadians) * r + wheelCenterX
        val y = sin(hueRadians) * r + wheelCenterY

        thumbView.setThumbCoordinate(x, y)
    }

    // For getting color from hue and saturation
    private fun getColor(hue: Float, saturation: Float): Int {
        return Color.HSVToColor(floatArrayOf(
            hue,
            saturation,
            1f
        ))
    }

    // Public methods

    fun setSelectedColor(color: Int) {
        val hsv = floatArrayOf(0f, 0f, 1f)
        Color.colorToHSV(color, hsv)
        setThumbCoordinate(hsv[0], hsv[1])
        val thumbColor = getColor(hsv[0], hsv[1])
        thumbView.setThumbColor(thumbColor)
    }

    interface OnColorWheelChange {
        fun onColorChange(color: Int)
    }
}